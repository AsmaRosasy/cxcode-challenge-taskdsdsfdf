# Code Challenge Task	

a simple dictionary key/value store script using only core NodeAPI and ECMAScript 5 or 6.

## Getting Started

These instructions will get you a copy of the project up and running on your local machine for development and testing purposes. See deployment for notes on how to deploy the project on a live system.

### Prerequisites

node 

Docker

### Running


Store Commands :

$ node store.js add mykey myvalue  or ./store.js add my key myvalue

$ node store.js list	    	    or ./store.js list

$ node store.js get mykey          or ./store.js get mykey

$ node store.js remove mykey       or ./store.js remove mykey

$ node store.js clear              or ./store.js clear

_____________________________________________________

To use it deploy and run it in docker container : 

$ docker build -t my-node-app .

$ sudo docker run my-node-app <command>

note : write function don't work on Docker , mostly probably due to permissions.





